﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

WebApplication app = builder.Build();

app.UseMiddleware<JWTAuthenticateMiddleware>(); // middleware này sẽ kiểm tra user có được phép truy cập

app.MapControllers();

app.Run();


[ApiController]
[Route("[controller]/[action]")]
public class UsersController : ControllerBase
{
    [HttpPost]
    public string FakeLoginToGenerateToken(User user)
    {
        // Tạo ra claim giả
        List<Claim> claims = new List<Claim>
        {
            new Claim(ClaimTypes.NameIdentifier, user.Username),
            new Claim(ClaimTypes.Role, "Chúa tể")
        };

        // Tạo ra token dựa trên dữ liệu giả
        string jwtToken = GenerateJwtToken(claims);
        return jwtToken;
    }

    public string Authenticate() => "you are my users";

    private string GenerateJwtToken(List<Claim> claims)
    {
        const string secretKey = "01234567890123456789012345678912"; // 256 bit hoặc 32 byte
        byte[] secretInBytes = Encoding.UTF8.GetBytes(secretKey);
        SymmetricSecurityKey securityKey = new SymmetricSecurityKey(secretInBytes);
        SigningCredentials credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        JwtSecurityToken token = new JwtSecurityToken(
            claims: claims,
            signingCredentials: credentials
        );

        JwtSecurityTokenHandler jwtTokenHandler = new JwtSecurityTokenHandler();
        string jwtToken = jwtTokenHandler.WriteToken(token);
        return jwtToken;
    }
}

public class User
{
    public string Username { get; set; }
}
