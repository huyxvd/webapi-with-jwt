﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;

public class JWTAuthenticateMiddleware
{
    private readonly RequestDelegate _next;

    public JWTAuthenticateMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        // Nếu mà fake data thì cho qua
        if (context.Request.Path.StartsWithSegments("/Users/FakeLoginToGenerateToken"))
        {
            await _next(context);
            return;
        }

        // kiểm tra header có chứa key là Authorization hay không vì jwt sẽ được gửi cùng key này
        if (!context.Request.Headers.ContainsKey("Authorization"))
        {
            // Chưa có key đi kèm
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            await context.Response.WriteAsync("please include Authorization key in header & value is JWT key");
            return;
        }

        try
        {
            // get the key from header
            string authHeader = context.Request.Headers["Authorization"].ToString();

            // read claim from jwt
            ClaimsPrincipal claimsPrincipal = GetClaimPrincipal(authHeader); // validate jwt
            context.User = claimsPrincipal; // gắn nó vào user trong httpcontext
        }
        catch (Exception)
        {
            // Kiểm tra jwt token
            context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            await context.Response.WriteAsync("JWT invalid");
            return;
        }
        // Nếu token được gắn ngon lành sẽ move qua middleware tiếp theo để làm việc khác
        await _next(context);
    }

    private ClaimsPrincipal GetClaimPrincipal(string jwtToken)
    {
        const string secretKey = "01234567890123456789012345678912"; // 256 bit hoặc 32 byte
        byte[] secretInBytes = Encoding.UTF8.GetBytes(secretKey);
        SymmetricSecurityKey securityKey = new SymmetricSecurityKey(secretInBytes);

        TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false, // set to true if you want to validate issuer
            ValidateAudience = false, // set to true if you want to validate audience
            ValidateLifetime = false, // set to true if you want to validate expiration
            IssuerSigningKey = securityKey
        };

        JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
        return tokenHandler.ValidateToken(jwtToken, tokenValidationParameters, out _);
    }

}
