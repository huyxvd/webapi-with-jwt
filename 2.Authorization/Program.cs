var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();

var app = builder.Build();

app.UseMiddleware<JWTAuthenticateMiddleware>();

app.MapControllers();

app.Run();
