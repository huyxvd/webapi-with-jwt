﻿using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Identity;

[ApiController]
[Route("[controller]/[action]")]
public class UsersController : ControllerBase
{
    [HttpPost]
    public string FakeLoginToGenerateToken(User user)
    {
        List<Claim> claims = new List<Claim>
        {
            new Claim(ClaimTypes.NameIdentifier, user.Username),
            new Claim(ClaimTypes.Role, user.Role)
        };

        var jwtToken = GenerateJwtToken(claims);
        return jwtToken;
    }

    [CustomAuthorize("admin")]
    public string HelloAdmin() => "hello admin";

    [CustomAuthorize("guest")]
    public string HelloGuest() => "hello guest";

    private string GenerateJwtToken(List<Claim> claims)
    {
        const string secretKey = "01234567890123456789012345678912"; // 256 bit hoặc 32 byte
        byte[] secretInBytes = Encoding.UTF8.GetBytes(secretKey);
        SymmetricSecurityKey securityKey = new SymmetricSecurityKey(secretInBytes);
        SigningCredentials credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        JwtSecurityToken token = new JwtSecurityToken(
            claims: claims,
            signingCredentials: credentials
        );

        JwtSecurityTokenHandler jwtTokenHandler = new JwtSecurityTokenHandler();
        string jwtToken = jwtTokenHandler.WriteToken(token);
        return jwtToken;
    }
}

public class User
{
    public string Username { get; set; }
    public string Role { get; set; }
}
