﻿using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;

[ApiController]
[Route("[controller]/[action]")]
public class UsersController : ControllerBase
{
    private readonly AppSettingConfiguration _appSettingConfiguration;

    public UsersController(AppSettingConfiguration appSettingConfiguration)
    {
       _appSettingConfiguration = appSettingConfiguration;
    }

    [HttpPost]
    [AllowAnonymous] // sử dụng cho việc đăng nhập
    public string FakeLoginToGenerateToken(User user)
    {
        List<Claim> claims = new List<Claim>
        {
            new Claim(ClaimTypes.NameIdentifier, user.Username),
            new Claim(ClaimTypes.Role, user.Role)
        };

        string jwtToken = GenerateJwtToken(claims);
        return jwtToken;
    }

    [HttpGet]
    [Authorize(Roles = "admin")]
    public string HelloAdmin() => "hello admin";

    [HttpGet]
    [Authorize(Roles = "guest")]
    public string HelloGuest() => "hello guest";


    [HttpGet]
    [Authorize(Roles = "guest,admin")]
    public string HelloGuestOrAdmin() => "hello guest or admin";

    private string GenerateJwtToken(List<Claim> claims)
    {
        byte[] secretInBytes = Encoding.UTF8.GetBytes(_appSettingConfiguration.JWTSection.SecretKey);
        SymmetricSecurityKey securityKey = new SymmetricSecurityKey(secretInBytes);
        SigningCredentials credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        JwtSecurityToken token = new JwtSecurityToken(
            issuer: _appSettingConfiguration.JWTSection.Issuer,
            audience: _appSettingConfiguration.JWTSection.Audience,
            expires: DateTime.UtcNow.AddMinutes(_appSettingConfiguration.JWTSection.ExpiresInMinutes),
            claims: claims,
            signingCredentials: credentials
        );

        JwtSecurityTokenHandler jwtTokenHandler = new JwtSecurityTokenHandler();
        string jwtToken = jwtTokenHandler.WriteToken(token);
        return jwtToken;
    }
}

public class User
{
    public string Username { get; set; }
    public string Role { get; set; }
}
