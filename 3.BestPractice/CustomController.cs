﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

[ApiController]
[Route("[controller]")]
public class CustomController : ControllerBase
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public CustomController(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    [HttpGet]
    [Authorize(Roles = "admin")]
    public IActionResult GetCurrentUser()
    {
        // Lấy User (ClaimsPrincipal) từ HttpContext
        ClaimsPrincipal user = _httpContextAccessor.HttpContext.User;

        // Lấy NameIdentifier từ danh sách các Claim
        string nameIdentifier = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;

        return Ok(nameIdentifier);
    }
}
