## import file post man in root project to getting start

## project 1 goal
1. understand the what is jwt
2. how jwt work
3. how to generate jwt token
4. how to validate it using custom middleware

## project 2 goal
1. extract the jwt token from header of http request
2. learn how to use `CustomAuthorizeAttribute` to validate the role of user

## project 3 goal
1. using build-in middleware from `Microsoft.AspNetCore.Authentication.JwtBearer` package
2. config validate middleware option issuer, audient, expires in minutes
3. dynamic load the configuration from enviroment

## what you will do
1. try to create to do app with 2 entity TodoTask, Users
2. Normal User can create & update the TodoTask
3. Admin User can view every one TodoTask
4. everyone can view TodoTask by UserId
5. allow admin enable/disable block `specific user` to create new todo task
